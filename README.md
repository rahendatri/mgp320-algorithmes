# MGP320 - Algorithmes

Implémentation et comparaison des algorithmes de traitement de flux de données

## Étapes :
1. Implémenter la version initiale (en comptant du premier élément)
    * [ ]  SaveSpacing (Joseph)
    * [X]  CMS (Rahenda)
2. Réadapter les algorithmes en fenêtre glissante
    * [ ]  SaveSpacing (Joseph)
    * [X]  CMS - Proportinal sliding window (Rahenda)
    * [ ]  CMS - Splitter sliding window (Rahenda)
    * [X]  Topkapi (Proportional) (Rahenda)
    * [ ]  Topkapi (fenêtre sautante -on remet à 0 à chq début de fenêtre)
2. Faire des tests et relever des courbes sur Topkapi en proportional sliding window
    * [X]  Implémenter la version « omnisciente » qui renvoit les valeurs réelles
    * Courbes :
        * [X] Faux positifs(taille du flux)
        * [X] Faux positifs(k)
        * [X] Précision(k)
        * [ ] ...
3. Un rapport technique de 15 pages maximum contenant (voir fiche détaillée sur Moodle) :
    * Le contexte du projet et sa problématique
    * La démarche suivie et le planning Gantt actualisé
    * La description des développements et le bilan critique du travail et des résultats
4. Une soutenance orale visant un jury technique non-spécialiste du domaine concerné :
    * 15 minutes de présentation avec prise de parole de chacun (démonstration appréciée)
    * 20 minutes de questions

# Quelques courbes
1. Faux positifs(taille du flux)
![alt text](https://gitlab.com/rr1996/mgp320-algorithmes/-/raw/master/false%20positive(flow%20size).png "Logo Title Text 1")

2. Faux positifs(k) i.e. k=3 veut dire que l'on veut les top 3 items
![alt text](https://gitlab.com/rr1996/mgp320-algorithmes/-/raw/master/false%20positive(k).png "Logo Title Text 1")

3. Faux positifs(k) i.e. k=3 veut dire que l'on veut les top 3 items
![alt text](https://gitlab.com/rr1996/mgp320-algorithmes/-/raw/master/Precision_k.png "Logo Title Text 1")


# Références :
1.  [Moodle MGP321](https://moodle.imt-atlantique.fr/course/view.php?id=596)
