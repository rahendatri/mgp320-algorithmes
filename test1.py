#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 24 15:51:21 2020

@author: Rahenda Rosmedi
TO BE CONTINUED
This test is to generate a graph Error(temps)
Precision = 1 - Error
It launches an execution and a query is sent every MESURE_STEP items
"""
import numpy as np
import matplotlib.pyplot as plt

import Topkapi as algo
import NaiveCounter as nc

FLOW_FILE = "2_flow_3000_items_0_to_20_alpha_1.txt"

# The following variable means : a measure is taken every 150 items
MESURE_STEP = 150


def main():
    current_step = 1 # trace the current step while inspecting the flow
    
    # x axis
    steps = []
    # y axis
    error = []
    
    # Get the flow from a file
    flow = retrieve_flow(FLOW_FILE)
        
    # BEGIN TEST
    # Launch for a fixed k
    param_k = 8
    param_n = 150
    param_epsilon = 0.1
    param_delta = 0.001
    sketch1 = algo.Topkapi(relative_error=param_epsilon, error_probability=param_delta, window_size=param_n, k=param_k)
    sketch2 = algo.Topkapi(relative_error=param_epsilon, error_probability=param_delta, window_size=param_n, k=param_k)
    sketch3 = algo.Topkapi(relative_error=param_epsilon, error_probability=param_delta, window_size=param_n, k=param_k)
    sketch4 = algo.Topkapi(relative_error=param_epsilon, error_probability=param_delta, window_size=param_n, k=param_k)
    
    # Begin flow inspection
    for item in flow :
        sketch1.update(item)
        sketch2.update(item)
        sketch3.update(item)
        sketch4.update(item)
        
        # Query every MESURE_STEP items
        if current_step % MESURE_STEP == 0:
            # Compute the heavy hitters    
            sketch1.compute_heavy_hitters()
            sketch2.compute_heavy_hitters()
            sketch3.compute_heavy_hitters()
            sketch4.compute_heavy_hitters()
             
            # Instanciate the naive counter
            naive_counters = nc.NaiveCounter()
            # Count last N elements with the naive counter to have the real result 
            for item1 in flow[(current_step-MESURE_STEP):current_step] :
                naive_counters.count(item1)
            
            # Get Topkapi heavy hitters
            topkapi_result1 = sketch1.get_heavy_hitters()
            topkapi_result2 = sketch2.get_heavy_hitters()
            topkapi_result3 = sketch3.get_heavy_hitters()
            topkapi_result4 = sketch4.get_heavy_hitters()
            
            # Get Naive counter heavy hitters (stored in real_topk)
            real_counters = naive_counters.get_counters()
            real_topk_indexes = np.where(real_counters[:]['lhh_count'] > (param_n/param_k))
            real_topk = real_counters[real_topk_indexes]
            
            
            # Compute the Topkapi error for the recent elements. We consider the mean number of detected heavy hitters by Topkapi..
            mean_topkapi_detected = (topkapi_result1.shape[0] + topkapi_result2.shape[0] + topkapi_result3.shape[0] + topkapi_result4.shape[0])/4
            real_detected = real_topk.shape[0]
            print(" ... : " + str(topkapi_result1))
            print(" +++ : " + str(real_topk))
            print(" ()+++ : " + str(real_counters))
            print(" CURRENT_STEP : " + str(current_step))
            print(" THRESHOLD :" + str (param_n/param_k))
            #error.append(abs((mean_topkapi_detected - real_detected)/mean_topkapi_detected))
            error.append(abs((mean_topkapi_detected - real_detected)/real_detected))
            

            steps.append(current_step)
        current_step += 1
        
    plt.title('error(nth item) : k={0} N={1} Ɛ={2} δ={3}'.format(param_k,param_n,param_epsilon,param_delta))
    plt.xlabel('nth item')
    plt.ylabel('error')
    plt.plot(steps, error,'o-')
    
    plt.show()


def retrieve_flow(file):
    items = []
    # Open the flow file and retrieve all items
    with open(file, "r") as f:
        list_of_items = f.readlines()
        # Convert the flow to list
        for g in list_of_items :
            # Convert to list
            items.append(int(g))
    return items


if __name__ == "__main__":
    main()




