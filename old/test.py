#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 15:13:32 2020

@author: Rahenda Rosmedi

This test is to generate a graph false positive(flow size)
"""
import numpy as np
import matplotlib.pyplot as plt

import Topkapi as algo
import NaiveCounter as nc

FLOWS_FILE = "generated_flows_2.txt"

# x and y axis
false_positives = []
flow_sizes = []

# the different flows
flows = []

# Open the flows file and retrieve all flows
with open(FLOWS_FILE, "r") as f:
    list_of_flows = f.readlines()
    
    # Convert the flows to list
    for g in list_of_flows :
        # Convert to list
        flow = list(g)
        
        # Delete the newline char
        del flow[-1]
        flow_int = [int(a) for a in flow]
        flows.append(flow_int)
    
for each_flow in flows :
    # Instanciate the Topkapi sketch
    param_k = 3
    param_n=10
    sketch = algo.Topkapi(0.1, 0.001, window_size=param_n, k=param_k)
    
    # Instanciate the naive counter
    naive_counters = nc.NaiveCounter()
    
    # Begin flow inspection
    for item in each_flow :
        sketch.update(item)
    
    # Compute the heavy hitters    
    sketch.compute_heavy_hitters()

    # Count last N elements with the naive counter to have the real result 
    for item1 in each_flow[-param_n:] :
        naive_counters.count(item1)

    # Get all results
    topkapi_result = sketch.get_heavy_hitters()
    real_counters = naive_counters.get_counters()
    real_topk_indexes = np.where(real_counters[:]['lhh_count'] > (param_n/param_k))
    real_topk = real_counters[real_topk_indexes]

    # Compute the false positive result for the current flow
    false_positives.append(abs(topkapi_result.shape[0] - real_topk.shape[0]))
    
    # Count how many elements in this flow
    flow_sizes.append(len(each_flow))
    
#print("--------------ITEM STORAGE--------------")
#print(item_storage)
#print("--------------ALL TOP K COUNTERS--------------")
#print(counters)
#print("--------------REAL TOP K--------------")
#print(real_topk)
#print("--------------TOPKAPI RESULT--------------")
#print(topkapi_result)
    
print("--------------FALSE POSITIVE--------------")
print(false_positives)
print("--------------FLOW SIZE--------------")
print(flow_sizes)

plt.plot(flow_sizes, false_positives)

plt.show()