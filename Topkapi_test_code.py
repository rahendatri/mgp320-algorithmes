#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 24 17:07:24 2020

@author: vagrant
"""

from Topkapi import Topkapi


g = Topkapi(0.1,0.001,[1]*100+[2]*10+[3]*3,k=3) #Thresold = 3.333 (10/3)
g.count_input_data()
g.compute_heavy_hitters()
g.compute_heavy_hitters()

heavy_hitter = g.get_heavy_hitters()
if tuple(heavy_hitter[0])[1] == 2:
    print(g.get_heavy_hitters())
    print("TEST OK")
else :
    print(g.get_heavy_hitters())
    print("!!!    TEST FAILED    !!!")