#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 21 14:00:33 2020

@author: Rahenda Rosmedi
"""

from CountMinSketch import CountMinSketch

import numpy as np
import copy
import math
import random

'''
Inheritance is inspired by this post :
https://www.digitalocean.com/community/tutorials/understanding-class-inheritance-in-python-3
'''
class SplitterWindowedCountMinSketch (CountMinSketch):
    
    def __init__(self, relative_error=None, error_probability=None, 
                 input_data=None, window_size=10, tau=None, mu=None) :
        '''
        Extended CMS class using splitter windowed method
        tau = minimum length ratio of subcell
        mu = amount of affordable information loss
        
        A new array of subcells is introduced. A subcell has the following structure :
        [initial step when the counter is updated, last step when the counter is updated, the counter]
        '''
        
        super().__init__(relative_error, error_probability, input_data)
        
        if not (tau>0 and tau<=1) :
            self.tau = 0.1
        else :
            self.tau = tau
            
        if mu is None :
            self.mu = 1.5
        else :
            self.mu = mu
        
        # This is used to track the current item within the window
        self.step = 0
        
        self.window_size = window_size
        
        # cm_subcells store all subcells of each count(i,j).
        # We use an array of objects in Python.
        # The array size is the same as the cm sketch size.
        # Each cell contains a queue of subcells.
        self.cm_subcells = np.zeros((2))
        
        
    def update(self, item, count=1):
        '''
        Update the CMS and subcells
        '''
        # A new element arrives, increment the step
        self.step += 1
        
        # Update the counter of item count(i,j) = count(i,j) - snapshot(i,j)
        for i in range(self.depth):
            item_hash = self._hash_item(item, i)
            
#            subcells = self.cm_subcells[i][item_hash]
            
            
            
            self.cm_sketch[i][item_hash] += count
            
            # If there is no subcell enqueued yet, then enqueue a new subcell
            if (self.cm_subcells[i][item_hash] == None) :
                # Double square brackets is needed based on this discussion : https://stackoverflow.com/questions/42545171/how-to-append-a-numpy-array-to-a-numpy-array
                subcell = np.array([ [self.step, self.step, 1] ])
                self.cm_subcells[i][item_hash] = subcell
            
            # If there are some subcells, get the latest one with -1 and compare its counter with the thresold
            elif (self.cm_subcells[i][item_hash][-1][2] < self.tau*self.window_size/self.width) :
                # if the thresold is not yet reached, increment the counter of the subcell and update the last step.
                self.cm_subcells[i][item_hash][-1][2] += 1
                self.cm_subcells[i][item_hash][-1][1] = self.step
            else :
                #TODO ERROR FUNCTION ???
                
                
    
    def __str__(self):
        res = ''' Sketch depth (i.e. number of hash functions) : {0}
        \n Sketch width (i.e. number of hash function possible values) : {1}
        \n CM Sketch snapshot : \n {2} 
        '''.format(len(self.cm_sketch), len(self.cm_sketch[0]), self.cm_sketch)
        return (res)