#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 15:57:13 2020

@author: Rahenda Rosmedi
"""

import numpy as np

class NaiveCounter (object):
    
    
    def __init__(self):
        # Instantiate a naive counters
        self.__counters = np.array([], dtype=[('lhh_count','float'), ('lhh','int')])
        
    def count(self, item):
        
        # If there is no counter yet for the item, add one
        if not np.any(self.__counters[:]['lhh'] == item):
            self.__counters = np.insert(self.__counters, self.__counters.shape[0], (1,item))
        else:
            # find the location where the element is stored
            item_indice = np.where(self.__counters[:]['lhh'] == item)
            
            # update its counter
            item_new_counter = self.__counters[item_indice]['lhh_count'] + 1
            
            # update the counters array
            np.put(self.__counters, item_indice, (item_new_counter, item))
        
    def get_counters(self):
        '''
        Get the sorted counters
        '''
        
        return(np.sort(self.__counters, kind='mergesort', order="lhh_count"))
        
        