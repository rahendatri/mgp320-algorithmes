#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 11:01:06 2020

@author: Rahenda Rosmedi

This test is to generate a graph Error(k)
Precision = 1 - Error
It launches an execution and a query is sent every MESURE_STEP items
"""
import numpy as np
import matplotlib.pyplot as plt

import Topkapi as algo
import NaiveCounter as nc

FLOW_FILE = "2_flow_3000_items_0_to_100_alpha_1.txt"

# The following variable means : a measure is taken every 150 items
MESURE_STEP = 3000
SKETCH_QUANTITY = 10

def main():
    # x axis
    all_k = []
    # y axis
    error = []
    
    # Get the flow from a file
    flow = retrieve_flow(FLOW_FILE)
        
    # BEGIN TEST
    for k in range(15,1000,5):
        sketch_instances = []
        topkapi_results = []
        
        current_step = 1 # trace the current step while inspecting the flow
        param_k = k
        param_n = MESURE_STEP
        param_epsilon = 0.01
        param_delta = 0.001
        for n in range(SKETCH_QUANTITY):
            sk = algo.Topkapi(relative_error=param_epsilon, error_probability=param_delta, window_size=param_n, k=param_k)
            sketch_instances.append(sk)
    
        # Begin flow inspection
        for item in flow :
            for each_sketch in sketch_instances :
                each_sketch.update(item)
            
            # Query every MESURE_STEP items
            if current_step % MESURE_STEP == 0:
                # Compute the heavy hitters    
                for each_sketch1 in sketch_instances :
                    each_sketch1.compute_heavy_hitters()
                
                # Instanciate the naive counter
                naive_counters = nc.NaiveCounter()
                # Count last N elements with the naive counter to have the real result 
                
                for item1 in flow[(current_step-MESURE_STEP):current_step] :
                    naive_counters.count(item1)
                
                # Get Topkapi heavy hitters
                for each_sketch2 in sketch_instances :
                    topkapi_results.append(each_sketch2.get_heavy_hitters())
                
                # Get Naive counter heavy hitters (stored in real_topk)
                real_counters = naive_counters.get_counters()
                real_topk_indexes = np.where(real_counters[:]['lhh_count'] > (param_n/param_k))
                real_topk = real_counters[real_topk_indexes]
                
                
                # Compute the Topkapi error for the recent elements. We consider the mean number of detected heavy hitters by Topkapi..
                total_detected = 0
                for each_topkapi_result in topkapi_results :
                    total_detected += each_topkapi_result.shape[0]
                mean_topkapi_detected = total_detected/SKETCH_QUANTITY
                real_detected = real_topk.shape[0]
                print(" ... : " + str(len(topkapi_results[0])))
                print(" +++ : " + str(len(real_topk)))
                #print(" []+++ : " + str(flow[(current_step-MESURE_STEP):current_step]))
                #print(" ()+++ : " + str(real_counters))
                print(" CURRENT_STEP : " + str(current_step))
                print(" THRESHOLD :" + str (param_n/param_k))
                #error.append(abs((mean_topkapi_detected - real_detected)/mean_topkapi_detected))
                err = abs((mean_topkapi_detected - real_detected)/real_detected)
                error.append(err)
                
    
                all_k.append(k)
            current_step += 1
        
    plt.title('error(k) : N={} Ɛ={} δ={} flow size={}'.format(param_n,param_epsilon,param_delta,len(flow)))
    plt.xlabel('k')
    plt.ylabel('error')
    plt.plot(all_k, error,'.-')
    
    plt.show()


def retrieve_flow(file):
    items = []
    # Open the flow file and retrieve all items
    with open(file, "r") as f:
        list_of_items = f.readlines()
        # Convert the flow to list
        for g in list_of_items :
            # Convert to list
            items.append(int(g))
    return items


if __name__ == "__main__":
    main()




