#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
@author: Joseph Facelina
"""

import numpy as np

class SpaceSaving (object):
    #Theta and precision will be needed for the treshold of frequent items. Handling of the data type to be done.
    def __init__(self, input_data=None, theta=None, precision=None, k=None) :
        if input_data is None :
            self.input_data = np.array([1,2,2,1,3,1,2,2,4,4,2,5,3])
        else :
            self.input_data = input_data
        
        if theta is None :
            self.theta = 0.001
        else :
            self.theta = theta
            
        if precision is None :
            self.precision = 0.0001
        else :
            self.precision = precision
            #k is the number of more frequent elements you want to track.
        if k is None :
            self.k = 3
        else :
            self.k = k
        
        self.counter = np.array([[0]*self.k]*3)
    
    def updateCounter(self):
        for element in self.input_data:
            countID = np.where(self.counter[0]==element)
            emptyCount = np.where(self.counter[1]==0)
            #Case 1, element already monitored, increase the count by 1.
            if countID[0].size > 0:
                self.counter[1][countID[0][0]]+=1             
            #Case 2, new element, with a free space, set the count to 1
            elif emptyCount[0].size > 0:
                self.counter[0][emptyCount[0][0]]=element
                self.counter[1][emptyCount[0][0]]=1
            #Case 3, new element, the previous element with lower count is replaced, count incremented, and previous count set as possible error.
            else:
                self.counter[0][-1]=element
                self.counter[2][-1]=self.counter[1][-1]
                self.counter[1][-1]+=1
            #The array is sorted every loop. Look for a more optimal way to preserve performances.
            self.counter = self.counter[:,self.counter[1,:].argsort()[::-1]]
            
                
               
            

