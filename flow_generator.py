#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 16:22:45 2020

@author: Rahenda ROSMEDI
Flow generator
"""

import zipf_gen_v2 as zfg


# Instantiate the generator
zipfg = zfg.ZipfGen(10,1)

#flow_size = [i*10 for i in range(1,100)]
flow_size = 3000


    
with open("2_flow_3000_items_alpha_1.txt","w") as f:
    # Generate a flow for each flow size
    for i in range(flow_size):
        f.write(str(zipfg.draw()))
    
        f.write("\n")
 
